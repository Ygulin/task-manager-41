package ru.tsc.gulin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}

