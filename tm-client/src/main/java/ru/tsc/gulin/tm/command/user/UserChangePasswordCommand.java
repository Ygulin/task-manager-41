package ru.tsc.gulin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.UserChangePasswordRequest;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change user password";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserEndpoint().changeUserPassword(new UserChangePasswordRequest(getToken(), password));
    }

}
