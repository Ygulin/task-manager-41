package ru.tsc.gulin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.gulin.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @Insert("INSERT INTO TM_TASK (id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt)"
            + " VALUES (#{id}, #{name}, #{description}, #{status}, #{userId}, #{created}, #{dateStart}, #{dateEnd})"
    )
    void add(@NotNull final TaskDTO task);

    @Insert("INSERT INTO TM_TASK (id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt)"
            + " VALUES (#{id}, #{name}, #{description}, #{status}, #{userId}, #{created}, #{dateStart}, #{dateEnd})"
    )
    void addAll(@NotNull final Collection<TaskDTO> tasks);

    @Update("UPDATE TM_TASK SET name = #{name}, description = #{description}, status = #{status},"
            + " created_dt = #{created}, start_dt = #{dateStart}, end_dt = #{dateEnd} WHERE id = #{id}"
    )
    void update(@NotNull final TaskDTO task);

    @NotNull
    @Select("SELECT id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt FROM TM_TASK")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<TaskDTO> findAll();

    @NotNull
    @Select("SELECT id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt FROM TM_TASK"
            + " WHERE user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<TaskDTO> findAllByUserId(@Param("userId") @NotNull final String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<TaskDTO> findAllSorted(@NotNull final SelectStatementProvider selectStatementProvider);

    @Nullable
    @Select("SELECT id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt FROM TM_TASK"
            + " WHERE id = #{id}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    TaskDTO findOneById(@Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt FROM TM_TASK"
            + " WHERE id = #{id} AND user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    TaskDTO findOneByIdAndUserId(@Param("userId") @NotNull final String user_id, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_TASK WHERE id = #{id}")
    void remove(@NotNull final TaskDTO task);

    @Delete("DELETE FROM TM_TASK WHERE id = #{id} AND user_id = #{user_id}")
    void removeByUserId(@Param("user_id") @NotNull final String userId, @NotNull final TaskDTO task);

    @Delete("DELETE FROM TM_TASK WHERE id = #{id}")
    void removeById(@Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_TASK WHERE id = #{id} AND user_id = #{userId}")
    void removeByIdAndUserId(@Param("user_id") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_TASK")
    void clear();

    @Delete("DELETE FROM TM_TASK WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull final String userId);

    @Select("SELECT count(1) = 1 FROM TM_TASK WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull final String id);

    @Select("SELECT count(1) = 1 FROM TM_TASK WHERE id = #{id} AND user_id = #{userId}")
    boolean existsByIdAndUserId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Select("SELECT count(1) FROM TM_TASK")
    long getSize();

    @Select("SELECT count(1) FROM TM_TASK WHERE user_id = #{userId}")
    long getSizeByUserId(@Param("userId") @NotNull final String userId);

    @Select("SELECT id, name, description, status, user_id, project_id, created_dt, start_dt, end_dt FROM TM_TASK"
            + "WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<TaskDTO> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

}
