package ru.tsc.gulin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    List<M> findAll(@NotNull String userId, @Nullable Sort sort);

    void add(@NotNull String userId, @NotNull M model);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

}
